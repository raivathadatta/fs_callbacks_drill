let creaFolder = require('../problem1.js')
let fs = require('fs')



function createFile(path, cd) {
    try {
        for (let index = 0; index < path.length; index++) {
            fs.writeFile(path[index], "file created", (err) => {
                if (err) {
                    console.log(err)
                }
            })
            cd(path[index])
        }
    } catch (err) {
        console.log(err)
    }
}

function deleteFile(path) {
    try {
        fs.unlink(path, (err) => err ? console.log(err) : console.log(`${path} file deleted`))
    } catch (err) {
        console.log(err.message)
    }
}
creaFolder.problem1(createFile, deleteFile)
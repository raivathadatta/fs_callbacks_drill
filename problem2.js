
/*Problem 2:
    
Using callbacks and the fs module's asynchronous functions, do the following:
    1. Read the given file lipsum.txt
    2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
    3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
    4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
    5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/


const fs = require('fs')
let pathUpper = 'uppercase.txt'
let pathLower = 'lowercase.txt'
let fileName = 'fileName.txt'
let sortFile = 'sort.txt'
let path = 'lorem.txt'

function readFile(cd) {
    try {
        fs.readFile(path, 'utf-8', (err, data) => {
            if (err) {
                console.log(err)
            }
            else {//convert it to new file 
                console.log(data)
                uppercaseConvertion(data)
            }
        })
    } catch (error) {
        console.log(error);
    }
}

function uppercaseConvertion(data) {
    fs.writeFile(pathUpper, data.toUpperCase(), (err) => {
        if (err) {
            console.log(err)
        }
        fs.appendFile(fileName, pathUpper, (err) => {
            if (err) { console.log(err) }
            toLowerCaseAndsplit()
        })
    })

}


function toLowerCaseAndsplit() {
    fs.readFile(pathUpper, 'utf-8', (err, data) => {
        if (err) {
            console.log(err)
        } else {
            let lowerCaseData = data.toLowerCase().split(",")
            fs.appendFile(pathLower, JSON.stringify({ 'lowerCaseData': lowerCaseData }), (err) => {
                if (err) { console.log(err) }
                fs.appendFile(fileName, `,${pathLower}`, (err) => {
                    if (err) { console.log(err) }
                    sortContent()
                })

            })
        }
    })
}

function sortContent() {
    fs.readFile(pathLower, 'utf-8', (err, data) => {
        if (err) {
            console.log(err)
        }
        else {
            let sort = data.split(",").sort()
            fs.writeFile(sortFile, sort.toString(), (err) => {
                err ? console.log(err) : ''
            })
            fs.appendFile(fileName, `,${sortFile}`, (err) => {
                if (err) { console.log(err) }
            })
            deleteAllFiles()
        }
    })
}

function deleteAllFiles() {
    fs.readFile(fileName, (err, data) => {
        if(err){
            console.log(err)
        }
        let fileName = data.toString().split(',')
        fileName.forEach((element) => {
            fs.unlink(element, (err) => {
                if (err) {
                    console.log(err)
                }
            })
        });

    })
}


module.exports = { readFile: readFile }
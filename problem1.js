/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/

const fs = require('node:fs');
function problem1(cdCreate, cdDelete) {
    let folderName = 'random'
    let fileName = [`./${folderName}/random.json`, `./${folderName}/random2.json`, `./${folderName}/random3.json`]
    try {
        fs.mkdir(folderName, (err) => {
            console.log(err)
        });
        cdCreate(fileName, cdDelete)
    } catch (err) {
        console.error(err);
    }
}



module.exports = { problem1: problem1 }
